<?php
include 'data.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset='<?php echo ENCODING; ?>'>
    <title><?php echo SITE_TITLE; ?></title>
    <?php echo $style_and_scripts; ?>
</head>
<body>
<div class="continer">

    <div class="row header">
        <h1> اولین وبسایت من</h1>
        <?php
        if ( user_logged_In())
        {
            $userinfo=getUserInfo($_SESSION['user']);
            echo 'wellcome to my website  '.$userinfo['display_name'];

        }



        ?>

    </div>

    <div class="row">
        <div class="column column-25 sidebar">
            <div class="column widget">
                <?php if(!user_logged_In()): ?>
                <a href="login.php" class="button">ورود </a>
                <a href="register.php" class="button">ثبت نام </a>
                <?php  else:?>
                <a href="login.php?action=logout" class="button">خروج </a>

                <?php  endif; ?>

            </div>

            <?php foreach ($widgets as $widget): ?>
                <div class="column widget">
                    <div class="wgtTitle"> <?php echo $widget[0]; ?></div>
                    <div class="wgtBody"> <?php echo $widget[1]; ?></div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="column column-72 column-offset-3 content">

            <?php //foreach ($posts as $post): ?>
            <?php for ($i = $startIndex; $i < $startIndex + PAGE_SIZE; $i++): ?>
                <?php if (!empty($posts[$i])): ?>
                    <div class="column postBox">
                        <div class="postTitle">  <?php echo $posts[$i][0]; ?> </div>
                        <div class="postBody"><?php echo $posts[$i][1]; ?></div>
                    </div>
                <?php endif; ?>

            <?php endfor; ?>
            <?php //endforeach; ?>
            <?php for ($i = 1; $i <= $pageCount; $i++): ?>
                <a href="?page= <?php echo $i; ?> " class="button"><?php echo $i; ?></a>
            <?php endfor; ?>
            <div class="pagination">

            </div>


        </div>
    </div>
</div>


</body>
</html>