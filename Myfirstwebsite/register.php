<?php

include_once "connect_db.php";
$errmsg = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errmsg[] = "ابمبل اشتباه وارد شده است.";
    }
    //   check password

    checkPassword($_POST['password1'], $errmsg);

    if ($_POST['password1'] != $_POST['password2']) {
        $errmsg[] = "رمز و تکرار رمز یکسان نیست.";
    }
    if (empty($errmsg)) {

        $res = register($_POST['email'], $_POST['password1'], $_POST['displayName']);
        if ($res == -1)
        {
            $errmsg[] = "یک کاربر با این ایمیل قبلا در سایت ثبت نام کرده است. ";
        }
    }
}

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ثبت نام</title>
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/milligram-rtl.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="continer">
    <?php if($_SERVER['REQUEST_METHOD'] == 'POST'):   ?>
    <?php if(empty($errmsg)):   ?>
    <div class="centerBox success">
        ثبت نام شما با موفقیت انجام شد.
    </div>

    <?php else:  ?>

        <div class="centerBox error">
           <?php
           foreach ($errmsg as $item) {
               echo "$item <br>";

           }
           ?>
        </div>

    <?php endif;  ?>
    <?php endif;  ?>


    <div class="centerBox">
    <h3>ثبت نام در سایت</h3>


    <form action="register.php" method="post">
        <input type="text" name="displayName" placeholder="نام کامل شما"
               value="<?php echo $_POST['displayName'] ?? ""; ?>">
        <input type="text" class="ltr text-left" name="email" placeholder="ایمیل"
               value="<?php echo $_POST['email'] ?? ""; ?>">
        <input type="password" class="ltr text-left" name="password1" placeholder="رمز عبور">
        <input type="password" class="ltr text-left" name="password2" placeholder="تکرار رمز عبور">
        <input type="submit" value="ثبت نام">
    </form>


</div>
</div>

</body>
</html>