<?php
define("USER_ROLE_ADMIN", 'admin');
define("USER_ROLE_AUTHOR", 'author');
define("USER_ROLE_SUBSCRIBER", 'subscriber');

function getUsersInfo()
{
    global $conn;
    $sql = "select * from users ";
    $result = mysqli_query($conn, $sql);
    /*  while ($row = mysqli_fetch_array($result)) {
          var_dump($row);

      }*/

    return mysqli_fetch_all($result);


}

function getUserInfo($email)
{
    if (!userExists($email)) {
        return null;

    }
    global $conn;
    $sql = "select * from users where email='$email'";
    $result = mysqli_query($conn, $sql);
    /*  while ($row = mysqli_fetch_array($result)) {
          var_dump($row);

      }*/

    return mysqli_fetch_assoc($result);


}

function userExists($email)
{
    global $conn;
    $sql = "select count(*) as c from users where email='$email'";
    $result = mysqli_query($conn, $sql);
    $count = mysqli_fetch_assoc($result);
    return $count['c'];


}


function register($email, $password, $display_name, $role = USER_ROLE_SUBSCRIBER)
{
    if (userExists($email)) {
        return null;

    }
    global $conn;
    $passwordhash = password_hash($password, PASSWORD_BCRYPT);
    $sql = "INSERT INTO `users` 
               ( `email`, `password`, `display_name`, `role`)
                VALUES  ('$email', '$passwordhash','$display_name','$role')";
    $result = mysqli_query($conn, $sql);
    var_dump($result);

}


function checkPassword($pwd, &$errors)
{
    $errors_init = $errors;
    if (empty($pwd)) {
        $errors[] = "رمز عبور وارد نشده است.";
    }

    if (strlen($pwd) < 8) {
        $errors[] = "رمز عبور کوتاه است.";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors[] = "رمز عبور باید حداقل یک عدد را داشته باشد.";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors[] = " رمز عبور باید حداقل یک حرف را داشته باشد.";
    }

    return ($errors == $errors_init);
}

//login($_POST['email'],$_POST['password']);

function login($email, $password)
{
    if (!userExists($email)) {
        return -1;
    }

    $userinfo = getUserInfo($email);
    if (password_verify($password, $userinfo['password'])) {
        $_SESSION['user'] = $email;
        header('Location:index.php');
        return true;
    }

    return false;
}

function user_logged_In()
{
    return !empty($_SESSION['user']);
}

function logout()
{
    $_SESSION['user'] = '';

    unset($_SESSION['user']);

}



